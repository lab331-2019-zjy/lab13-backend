package se331.lab.rest.entity;

import lombok.*;

import lombok.experimental.SuperBuilder;
import se331.lab.rest.security.entity.User;

import javax.persistence.*;
import javax.persistence.Entity;

import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Data
@SuperBuilder
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @EqualsAndHashCode.Exclude
    Long id;
    String name;
    String surname;
    @OneToOne
    User user;


}
